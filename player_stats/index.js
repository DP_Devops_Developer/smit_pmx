const express = require('express');
const axios = require('axios');
const mysql = require('mysql');
const cors = require('cors');
const request = require('request');

const app = express();
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(cors());


const config = mysql.createConnection({
    host: 'localhost',
    user: 'root',
    password: '',
    database: 'player_states',
    multipleStatements: true
});

config.connect((error) => {
    if (error) throw error;
    console.log("Connected to player_states");
});


// localhost:8004//fetch/player/614/stats
app.post('update/player/:id/stats', async (req, res) => {
    const pid = parseInt(req.params.id);

    const sql = 'UPDATE states SET '
    res.send({ message: `Player ${pid} Updated` });
})

app.post('/fetch/player/:id/states', async (req, resp) => {
    const pid = parseInt(req.params.id);
    const url1 = `https://rest.entitysport.com/v2/players/${pid}/stats?token=0401628019fa4df1522947816389fdf6`

    request.get({ url: url1, json: true }, async (error, res, data) => {
        if (error) throw error;
        main = data.response;
        ptype = main.player.playing_role;
        fantasy_point = main.player.fantasy_player_rating;
        main_batting = Object.entries(main.batting).map((e) => ({ [e[0]]: e[1] }));
        main_bowling = Object.entries(main.bowling).map((e) => ({ [e[0]]: e[1] }));
        let test = [pid, ptype];
        let odi = [pid, ptype];
        
        let t20i = [pid, ptype];
        let t20 = [pid, ptype];
        let lista = [pid, ptype];
        let firstclass = [pid, ptype];
        let arr = [test, odi, t20i, t20, lista, firstclass]
        let fix = ['test', 'odi', 't20i', 't20', 'lista', 'firstclass']
        let missing = false;
        let tracer = {};
        let i = 0;
        await main_batting.forEach(element => {
            mtype = Object.keys(element)[0];
            while (i < fix.length) {
                if (mtype == fix[i]) {
                    arr[i].push(mtype);
                    mdata = Object.values(element)[0];
                    arr[i].push(parseInt(mdata.innings || 0));
                    arr[i].push(mdata.runs || 0);
                    arr[i].push(mdata.run4 || 0);
                    arr[i].push(mdata.run6 || 0);
                    arr[i].push(mdata.run100 || 0);
                    arr[i].push(parseFloat(mdata.strike || 0));
                    arr[i].push(mdata.stumpings || 0);
                    arr[i].push(mdata.catches || 0);
                    i = i + 1;
                    break;
                } else {
                    missing = true;
                    tracer[`${i}`] = fix[i];
                    console.log(tracer);
                    i = i + 1;

                }
            }
        });
         i = 0;
        await main_bowling.forEach(element => {
            
            mtype = Object.keys(element)[0];
            while (i < fix.length) {
                if (mtype == fix[i]) {
                    mdata = Object.values(element)[0];
                    arr[i].push(parseInt(mdata.wickets || 0));
                    arr[i].push(parseFloat(mdata.econ || 0));
                    arr[i].push(parseFloat(mdata.strike || 0));
                    arr[i].push(mdata.innings || 0);
                    i = i + 1;
                    break;
                } else {
                    i = i + 1;
                }
            }
        })
        console.log(missing, tracer, Object.keys(tracer).length);
        if (missing === true){
            toSliced = Object.keys(tracer).length;
            arr = arr.slice(toSliced,);
            
        }
        config.query("INSERT INTO player_states.states (player_id, player_type, match_type, innings, runs, four, six, century, strike_rate, stumping, catches, wickets, economy, strike, bowling_innings) VALUES ?", [arr], (error, result) => {
            if(error) throw error;
            console.log("Rows Added")
        });
        resp.status(200).send({message:"OK"});
    })

})

// (player_id, player_type, match_type, innings, runs, four, six, strike_rate, runout, stumping, catches, wickets, economy, strike, bowling_innings)


app.post('/player/:pid/stats', async (req, res) => {
    pid = parseInt(req.params.pid);
    const sql = `SELECT * FROM states WHERE player_id = ${pid}`;
    config.query(sql, (error, result) => {
        if (error) throw error
        res.status(200).send(JSON.parse(JSON.stringify(result)));
    })
    // res.status(200).send({message:'OK'})
})

app.listen(8004, () => {
    console.log("LISTENING ON PORT:8004")
})