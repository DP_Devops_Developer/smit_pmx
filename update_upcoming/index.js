const express = require('express');
const mysql = require('mysql');
const request = require('request');
const cors = require('cors');
const axios = require('axios');
// const {Kafka} = require('kafkajs');



const conn = mysql.createConnection({
    host:'localhost',
    user:'root',
    password:"",
    database:"upcoming_db_master",
    multipleStatements: true
})

conn.connect(function(err){
    if (err) throw err;
    console.log('connected')
});

const kafka = new Kafka({
    clientId:'receiver',
    brokers:['localhost:9092', 'localhost:9093', 'localhost:9094']
})

const consumer = kafka.consumer({groupId:'lineup', allowAutoTopicCreation:false});
consumer.connect().then(()=>console.log("Consumer Connected"));
consumer.subscribe({topic:'ContestCreated', allowAutoTopicCreation:false});
consumer.subscribe({topic:'LineupAvailable', allowAutoTopicCreation:false});

const app = express();
app.use(express.json());
app.use(cors());

id1 = 100;
id2 = 101;
id3 = 102; id4=103; id5=104; id6=105;
// calls entity sport api to fetch and store upcoming match data(first-10/first_page) and emmits events with match_ids. #consumed by contest-service, add_team-service
app.get('/update-upcoming-db',
     async (req, resp) => {
        
        // request.get({url:'https://rest.entitysport.com/v2/matches/?status=1&token=0401628019fa4df1522947816389fdf6', json: true}, async (err, res, data) =>{
        //     if(err) throw err;
        //     res_data = data.response.items;
        //     final = []
        //      var list_id = []
        //      res_data.forEach(element => {
        //         temp = [element.match_id, element.competition.cid, element.teama.name, element.teama.team_id, element.teama.logo_url, element.teamb.name, element.teamb.team_id,element.teamb.logo_url, element.timestamp_start, element.venue.timezone, element.pre_squad]
        //         final.push(temp)
        //         list_id.push(element.match_id)
        //     });
        //     console.log(list_id)
        //     console.log("Entering into sql...3.2.1");
        //     var sql = "INSERT INTO upcoming (match_id, competition_id, team1, team1_id, team1_logo, team2, team2_id, team2_logo, timestamp, timezon, line_up) VALUES ?";
        //     conn.query(sql, [final], function(err, result) {
        //         if (err) throw err;
        //         console.log("Number of records inserted: " + result.affectedRows);
                
        //     });
    
        //  await axios.post('http://localhost:11000/events', {type:"UpcomingUpdated",match_ids:list_id}); 
        //  resp.send(JSON.stringify({status:'OK'}));

        // });
        lid = [id1++,id2++,id3++,id4++,id5++,id6++];
        console.log(lid);
        await axios.post('http://localhost:11000/events', {type:"UpdatedUpcoming",data:{match_ids:lid}}); 
         resp.send({status:'OK'});
    
        // await axios.post('localhost:11000/events', {match_ids : list_id});
});




app.post('/events', async (req, res) =>{
    console.log("KAFKA IN UPDATE UPCOMING");

    async function run(){
        await consumer.run({
            eachMessage: ({message}) =>{
                console.log(JSON.parse(message.value));
            }
        })
    }
    run().then(()=>console.log("Received"), err => console.log(err));
})

// //  Receives event when contest/s created of particular match and updates database column "contest_available" to 1 #produced by contest-service
// app.post('/events', async (req, res)=>{
//     console.log('Event Received in UPDATE');
//     const {type, match_id} = req.body;
//     if(type == 'Contest-Created'){
//         console.log('Contest Created Event Received');
//         console.log(req.body);
//         const sql = `UPDATE upcoming SET contest_available = 1 WHERE match_id = ${match_id}`;
//         conn.query(sql, function (err, result) {
//             if (err) throw err;
//             console.log(result.affectedRows + " record(s) updated");
//           });
//         res.send({status:'OK'})
//     }
// })


app.listen(7001, ()=>{
    console.log('Listening on 7001')
}); 