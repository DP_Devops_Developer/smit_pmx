const express = require('express');
const cors = require('cors');
const axios = require('axios');
const {Kafka} = require('kafkajs');
const {UpdatedUpcoming,ContestCreated,CreatedUser,LineupAvailable,UserCreatedTeam,UserParticipated} = require('./events');


const kafka = new Kafka({
    brokers:['localhost:9092','localhost:9093', 'localhost:9094'],
});

const producer = kafka.producer();
producer.connect().then(
    console.log("Producer is Ready")
).catch(err =>{
    console.log(err);
})

// const produceMessage = async (topic, message) =>{
//     try{
//         obj = {topic:topic,
//             messages:[
//                 {key:'1',partition:0 ,value:JSON.stringify(message)},
//                 {key:'1',partition:1 ,value:JSON.stringify(message)},
                
//             ]

//         }
//         console.log("OBJECT TO BE PRODUCED", obj);
//         producer.send(obj)
//     }catch(err){
//         console.log(err)
//     }
// }


const app = express();
app.use(express.json());
app.use(cors());


app.post('/events', async (req, res) => {
    console.log("Event Bus Called");
   
    const event = req.body;
    console.log(event);
    
    // await producer.send({
    //     topic:req.body.type,
    //     messages:[
    //         {key:'data', value:JSON.stringify(req.body.data)}
    //     ]
    // });
    // await produceMessage(req.body.type, req.body.data);
    if(req.body.type == 'UpdatedUpcoming'){
     await UpdatedUpcoming(req.body.data);
    }
    if(req.body.type == 'UserCreatedTeam'){
     await  UserCreatedTeam(req.body.data);
    }
    if(req.body.type == 'ContestCreated'){
      await  ContestCreated(req.body.data);
    }
    if(req.body.type == 'CreatedUser'){
      await  CreatedUser(req.body.data);
    }
    if(req.body.type == 'UserParticipated'){
       await  UserParticipated(req.body.data);
    }
    if(req.body.type == 'LineupAvailable'){
      await  LineupAvailable(req.body.data);
    }
    
     axios.post('http://localhost:8002/events', req.body);
   
     axios.post('http://localhost:7001/events', req.body);
     axios.post('http://localhost:8003/events', req.body);
    
     axios.post('http://localhost:8008/events', req.body);
    //  axios.post('http://localhost:8009/events', req.body);
     axios.post('http://localhost:8010/events', req.body);


    res.send({status:'OK'});

});


app.listen(11000, ()=>{
    console.log("Listening on port 11000");
})